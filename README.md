**How to started**

Solo es necesario los siguientes pasos.

1. Crear la base de datos.
2. Colocar el proyecto en el webserver con PHP 7.0 y MySQL
3. Colocar las credenciales en el archivo **/database/Connection.php** .
4. Ejecutar el script de migracion por consola **php database/migration.php** Debe estar en el path del proyecto antes de ejecutar el comando.
5. Acceder al proyecto desde el navegador web.

---