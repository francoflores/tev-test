
function login(params) {
  $(".btn").attr("disabled", true);
  $.post("api/login.php", params, function(response) {
    console.log(response);
    if(response.success) {
      window.location.href = "views/users.html";
    } else {
      $("#msgCredentials").removeClass('d-none');
    }

    $(".btn").attr("disabled", true);
  }, 'json');
}

$(document).ready(function() {

  
  $("#formLogin").submit(function(event) {
    event.preventDefault();
    
    let params = {};
    $("#formLogin").serializeArray().forEach(element => {
      params[element.name] = element.value;
    });

    console.log(params);
    login(params)
  });
});