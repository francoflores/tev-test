<?php

  spl_autoload_register(function($name){

    if (file_exists("../controllers/$name.php")) {
      include "../controllers/$name.php";
    } 

    if (file_exists("../database/$name.php")) {
      include "../database/$name.php";
    } 
  });

  function login($email, $password) {
    $loginController = new LoginController();
    return $loginController->login($email, $password);
  }

  try {
    if(!isset($_SERVER["REQUEST_METHOD"])) {
      $data=array("status"=>"0","message"=>"Please enter proper request method !! ");
			echo json_encode($data);
    }
    else {
      $loginController = new LoginController();
      $method=$_SERVER["REQUEST_METHOD"];
      switch($method) {
        case "POST": 
          //echo json_encode(["result"=>"hola"]);
          $data = $loginController->login($_REQUEST['email'], $_REQUEST['password']); //login($_REQUEST['email'], $_REQUEST['password']);
          if($data != null) {
            session_start();
          }
          echo json_encode(["result"=> $data, "success"=> $data != null]);
          break;
        default:
          break;
      }
    }
    
  }
  catch(Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
  }
?>