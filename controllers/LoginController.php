<?php

class LoginController 
{
  public function login($email, $password)
  {
    $connection = new Connection();
    return $connection->login($email, $password);
  }
}
